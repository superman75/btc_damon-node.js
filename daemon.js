var assert = require('better-assert');
var async = require('async');
var bc = require('./src/bitcoin_client');
var db = require('./src/db');
var lib = require('./src/lib');
var fs = require('fs')


var client;

// Mapping of deposit_address -> user_id
var depositAddresses = null;
if (process.env.TESTNET == 1)
{
    depositAddresses = JSON.parse(fs.readFileSync('.addresses_testnet.json', 'utf8'));
}
else
{
    depositAddresses = JSON.parse(fs.readFileSync('.addresses.json', 'utf8'));
}
assert(depositAddresses);

startBlockLoop();

function processTransactionIds(txids, callback)
{
    bc.getTransactionIdsAddresses(txids, function(err, addressToAmountLists)
    {
        if (err) return callback(err);

        assert(txids.length === addressToAmountLists.length);

        var tasks = [];

        addressToAmountLists.forEach(function(addressToAmount, i)
        {
            var txid = txids[i];
            
            assert(txid);

            var usersToAmounts = {};

            Object.keys(addressToAmount).forEach(function(address)
            {
                var userId = depositAddresses[address];
                if (userId)
                {
                    usersToAmounts[userId] = addressToAmount[address];
                }
            });

            if (Object.keys(usersToAmounts).length > 0)
            {
                console.log('BTC: Transactions: ', txid, ' matches: ', usersToAmounts);

                Object.keys(usersToAmounts).forEach(function(userId)
                {
                    tasks.push(function(callback)
                    {
                        // WRT : insert to table:fundings , and update table:users : db
                        db.addDeposit(userId, txid, usersToAmounts[userId], callback);
                    });
                });
            }
        });

        async.parallelLimit(tasks, 3, callback);
    });
}

// Handling the block...

/// block chain loop

var lastBlockCount;
var lastBlockHash;

function startBlockLoop()
{
    // initialize...
    db.getLastBlock(function (err, block)
    {
        if (err)
        {
            throw new Error('Unable to get initial last block: ', err);
        }

        lastBlockCount = block.height;// block height : (block id:index) : ex): 	504809
        lastBlockHash = block.hash; // block hash : ex):	000000000000000000296bad123bcde8c889101cf3986df66dab81b578343332

        console.log('BTC: Initialized on block: ', lastBlockCount, ' : ', lastBlockHash);

        blockLoop();
    });
}

function scheduleBlockLoop()
{
    setTimeout(blockLoop, 20000); // every 20s
}

var g_nLastNum = 0;

function blockLoop()
{
    bc.getBlockCount(function(err, num)
    {
        if (err)
        {
            console.error('BTC: Unable to get block count');
            return scheduleBlockLoop();
        }

        if (num === lastBlockCount)
        {
            // last block has already been processed. there is nothing to do.
            if (num != g_nLastNum)
            {
                console.log('BTC: Block : ', num, ': loop ...');
                g_nLastNum = num;
            }

            return scheduleBlockLoop();
        }

        // here : num > lastBlockCount
        // WRT : need to Modify ?????
        // num : current very last block count : real
        // bc.getBlockHash(lastBlockCount, function(err, hash) - > bc.getBlockHash(num, function(err, hash)

        // if last block has not been processed. that is num != lastBlockCount and hash != lastBlockHash
        bc.getBlockHash(lastBlockCount, function(err, hash)
        {
            if (err)
            {
                console.error('BTC: Error: Get block hash: ' + err);
                return scheduleBlockLoop();
            }

            if (lastBlockHash !== hash)
            {
                // There was a block-chain reshuffle. So let's just jump back a block
                db.getBlock(lastBlockCount - 1, function(err, block)
                {
                    if (err)
                    {
                        console.error('BTC: ERROR: Unable jump back ', err);
                        return scheduleBlockLoop();
                    }

                    console.log("BTC: Jump block back: " + lastBlockCount - 1 );

                    --lastBlockCount;
                    lastBlockHash = block.hash;
                    blockLoop();
                });
                return;
            }

            bc.getBlockHash(lastBlockCount+1, function(err, hash)
            {
                if (err)
                {
                    console.error('BTC: Error: Get block hash: ', lastBlockCount+1);
                    return scheduleBlockLoop();
                }

                processBlock(hash, function(err)
                {
                    console.log('processblock');
                    if (err)
                    {
                        console.error('BTC: Error: Process block: ', hash, ' : ', err);
                        return scheduleBlockLoop();
                    }

                    ++lastBlockCount;
                    lastBlockHash = hash;

                    db.insertBlock(lastBlockCount, lastBlockHash, function(err)
                    {
                       if (err)
                          console.error('BTC: Danger, Save results in database...');

                        // All good! Loop immediately!
                        blockLoop();
                    });
                });
            });

        });
    });
}

function processBlock(hash, callback)
{
    // console.log('BTC: Processing block : ', hash);
    // hash = '00000000000216158256fc23cfca0b82d23f110a0de55037c715772e77834ca5';1275787

    var start = new Date();

    bc.getBlock(hash, function(err, blockInfo)
    {
        if (err)
        {
            console.error('BTC: Error: Get block info: ', hash, ' : ', err);
            return callback(err);
        }

        var transactionsIds = blockInfo.tx;

        console.log("BTC: Block processing : " + blockInfo.height);

        processTransactionIds(transactionsIds, function(err)
        {
            console.log('processTransactionIds');
            if (err)
                console.log('BTC: Error: Process block (in ',  (new Date() - start ) / 1000, ' seconds)');
//            else
//                 console.log('BTC: Processed ', transactionsIds.length, ' transactions in ', (new Date() - start ) / 1000, ' seconds');

            callback(err)
        });
    });
}
