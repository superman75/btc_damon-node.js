//postgresql API
var bc = require('./bitcoin_client');
var assert = require('better-assert');
var async = require('async');
var pg = require('pg');

var DB_URL_REMITANO = 'postgres://postgres:koguryo1998@localhost/remitanodb';
// postgres://postgres:koguryo1998@localhost/reminatodb
var DB_URL_BUSTABIT = 'postgres://postgres:koguryo1998@localhost/bustabitdb';

// var databaseUrl = process.env.DATABASE_URL;
var databaseUrl = DB_URL_REMITANO;
assert(databaseUrl);

// console.log('BTC: DATABASE_URL: ', databaseUrl);

pg.types.setTypeParser(20, function(val) { // parse int8 as an integer
    return val === null ? null : parseInt(val);
});


function refreshView() {

    // query('REFRESH MATERIALIZED VIEW CONCURRENTLY leaderboard;', function(err)
    // {
    //     if (err)
    //     {
    //         console.error('BTC: Error: Refresh leaderboard: ', err);
    //     } else {
    //         // console.log('BTC: Leaderboard refreshed');
    //     }

    //     setTimeout(refreshView, 10 * 60 * 1000);
    // });

}
setTimeout(refreshView, 1000); // schedule it so it comes after all the addresses are generated


// callback is called with (err, client, done)
function connect(callback) {
    return pg.connect(databaseUrl, callback);
}

function query(query, params, callback) {
    //thrid parameter is optional
    if (typeof params == 'function') {
        callback = params;
        params = [];
    }

    connect(function(err, client, done) {
        if (err) return callback(err);

        client.query(query, params, function(err, result) {
            done();
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
}

// runner takes (client, callback)

// callback should be called with (err, data)
// client should not be used to commit, rollback or start a new transaction


// callback takes (err, data)

function getClient(runner, callback)
{
    connect(function (err, client, done)
    {
        if (err) return callback(err);

        function rollback(err)
        {
            client.query('ROLLBACK', done);
            callback(err);
        }

        client.query('BEGIN', function (err) {
            if (err)
                return rollback(err);

            runner(client, function(err, data) {
                if (err)
                    return rollback(err);

                client.query('COMMIT', function (err) {
                    if (err)
                        return rollback(err);

                    done();
                    callback(null, data);
                });
            });
        });
    });
}

// callback is called with (err, client, done)
exports.getClient = function(callback) {
    var client = new pg.Client(databaseUrl);

    client.connect(function(err) {
        if (err) return callback(err);

        callback(null, client);
    });
};


/**********************************************************************
 * get last block from btc_blocks table
 * @param callback
 */
exports.getLastBlock = function(callback)
{
    query('SELECT * FROM btc_blocks ORDER BY height DESC LIMIT 1', function(err, results)
    {
        if (err)
        {// db error
            return callback(err);
        }

        if (results.rows.length === 0)
        {
            if (process.env.TESTNET == 1)
            {
                return callback(null, { height: 514, hash: '00000000040b4e986385315e14bee30ad876d8b47f748025b26683116d21aa65' }); // genesis block // test net // WRT
            }
            else
            {
                return callback(null, { height: 0, hash: '000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f' }); // genesis block // main net
            }

        }

        assert(results.rows.length === 1);// check LIMIT 1 : db error
        callback(null, results.rows[0]); // int4:height  text:hash from db:btc_blocks
    });
};

exports.getBlock = function(height, callback) {
    query('SELECT * FROM btc_blocks WHERE height = $1', [height], function(err, results) {
        if (err) return callback(err);

        if (results.rows.length === 0)
            return callback(new Error('Could not find block ' + height));

        // assert(results.rows.length === 1);
        // Orphaned blocks exist. so orphaned & mainchain blocks both exist.
        if (results.rows.length !== 1)
        {
            console.log('BTC: Orphan block found: ' + height);
            var nCntHashes = results.rows.length;

            for (var nId = 0 ; nId < nCntHashes ; nId ++)
            {
                bc.getBlock(results.rows[nId].hash, function(err, blockInfo)
                {
                    if (err || blockInfo.confirmations == -1 || blockInfo.nextblockhash == undefined)
                    {
                        query('DELETE FROM btc_blocks WHERE hash=$1', [results.rows[nId].hash], function(err)
                        {
                            if (err)
                            {
                                console.error('BTC: Error: Del orphaned block db: ', err);
                            }
                        });
                    }
                    else if (blockInfo.confirmations > 0 && blockInfo.nextblockhash)
                    {
                        console.log('BTC: Orphaned blocks: found mainchain block');
                        callback(null, results.rows[nId]);
                    }
                });
            }
        }
        else
        {
            callback(null, results.rows[0]);
        }
    });
};

exports.insertBlock = function(height, hash, callback) {
    query('INSERT INTO btc_blocks(height, hash) VALUES($1, $2)', [height, hash], callback);
};


//notifier is called with the row (bv_moneypots joined bv_user)



exports.addDeposit = function(userId, txid, amount_btc, callback)
{
    console.log('BTC: Trying to add deposit: ', userId, txid, amount_btc);

    assert(typeof amount_btc === 'number');
   // Amount is in bitcoins...
    var amount_satoshi = Math.round(amount_btc * 1e8);
    getClient(function(client, callback)
    {

        async.parallel([
            function(callback)
            {
                 var today = new Date();
                 client.query('INSERT INTO withdrawals(user_id, amount, txid, side, market, created_at) ' +
                              "VALUES($1, $2, $3, 'Deposit', 'BTC', $4)",
                              [userId, amount_btc, txid, today], callback);

                console.log("Deposit of BTC is successful");
            },
            function(callback)
            {
                 // client.query("UPDATE users SET balance_satoshis = balance_satoshis + $1 WHERE id = $2",
                 //              [amount_satoshi, userId], callback);
                 client.query("UPDATE users SET balance_btc = balance_btc + $1 WHERE id = $2",
                               [amount_btc, userId], callback);
                console.log("update user's table for btc")
            }],
            callback);

    },function(err)
    {
        // console.log('getClient error');
        if (err)
        {
            if (err.code == '23505')
            { // constraint violation
                console.log('BTC: Warning deposit constraint violation for (', userId, ',', txid, ')');
                return callback(null);
            }

            console.log('BTC: Error: Save: (', userId, ',', txid, ') : ', err);
            return callback(err);
        }

        // Ref fee
        // query("SELECT username,did_ref_deposit,ref_id FROM users WHERE id = $1", [userId], function(err, result)
        // query("SELECT * FROM users WHERE id = $1", [userId], function(err, result)
        // {
        //     if (err) return callback(err);

        //     if (result.rows[0].ref_id != "" && result.rows[0].did_ref_deposit == false)
        //     {
        //         var ref_id = result.rows[0].ref_id;
        //         var username = result.rows[0].username;
        //         var ref_deposit_fee = amount_satoshi * 0.05;
        //         ref_deposit_fee = parseInt(ref_deposit_fee);

        //         query("UPDATE users SET did_ref_deposit = true WHERE id = $1", [userId], function(err, result)
        //         {
        //             if (err) return callback(err);

        //             // send money to agent
        //             makeRefDepositFee(username, ref_id, ref_deposit_fee, function (err, result) {
        //                 if (err) return callback(err);
        //             });
        //         });
        //     }
        // });

        callback(null);
    });


};

makeRefDepositFee = function(fromUser, toUsername, satoshis, callback)
{
    console.log('ref_deposit_fee', satoshis);
    getClient(function(client, callback) {
        async.waterfall([
            function(callback) {
                client.query("UPDATE users SET balance_satoshis = balance_satoshis - $1 WHERE username = $2",
                    [satoshis, fromUser], callback)
            },
            function(prevData, callback) {
                client.query(
                    "UPDATE users SET balance_satoshis = balance_satoshis + $1 WHERE lower(username) = lower($2) RETURNING id",
                    [satoshis, toUsername], function(err, data) {
                        if (err)
                            return callback(err);
                        if (data.rowCount === 0)
                            return callback('USER_NOT_EXIST');
                        var toUserId = data.rows[0].id;
                        assert(Number.isInteger(toUserId));
                        callback(null, toUserId);
                    });
            }
        ], function(err) {
            if (err) {
                if (err.code === '23514') {// constraint violation
                    return callback('NOT_ENOUGH_BALANCE');
                }
                if (err.code === '23505') { // dupe key
                    return callback('TRANSFER_ALREADY_MADE');
                }

                return callback(err);
            }
            callback();
        });
    }, callback);

};