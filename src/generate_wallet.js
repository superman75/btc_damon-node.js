var bitcoinjs = require('bitcoinjs-lib');

var privKey = process.env.BIP32_PRIV;

//var hdNode = bitcoinjs.HDNode.fromBase58(privKey);

// WRT
var hdNode = bitcoinjs.HDNode.fromBase58(privKey, bitcoinjs.networks.bitcoin);

var count = process.env.GENERATE_ADDRESSES ? parseInt(process.env.GENERATE_ADDRESSES) : 100; // how many addresses to watch

var rescan = 'false';

for (var i = 1; i <= count; ++i) {
  console.log('bitcoin-cli -rpcuser=' + process.env.BITCOIND_USER + ' -rpcpassword=' + process.env.BITCOIND_PASS + ' importprivkey ' +  hdNode.derive(i).keyPair.toWIF() + " " + i + " " + rescan)
}
